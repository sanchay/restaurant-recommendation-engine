from enum import Enum


class Cuisine(Enum):
    SOUTH_INDIAN = "SOUTH_INDIAN"
    NORTH_INDIAN = "NORTH_INDIAN"
    CHINESE = "CHINESE"
