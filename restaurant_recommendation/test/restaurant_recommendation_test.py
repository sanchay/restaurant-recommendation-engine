import pytest
from datetime import date

from restaurant_recommendation.restaurant_recommendations import RestaurantRecommendations
from restaurant_recommendation.user import User
from restaurant_recommendation.cuisine import Cuisine
from restaurant_recommendation.cuisine_tracking import CuisineTracking
from restaurant_recommendation.cost_tracking import CostTracking
from restaurant_recommendation.restaurant import Restaurant


@pytest.fixture
def example_old_user_data():
    south_indian_cuisine_tracking = CuisineTracking(Cuisine.SOUTH_INDIAN, 10)
    north_indian_cuisine_tracking = CuisineTracking(Cuisine.NORTH_INDIAN, 8)
    cost_tracking_1 = CostTracking(1, 5)
    cost_tracking_3 = CostTracking(3, 7)
    cost_tracking_4 = CostTracking(4, 3)
    cost_tracking_5 = CostTracking(5, 3)
    return User([south_indian_cuisine_tracking, north_indian_cuisine_tracking],
                [cost_tracking_1, cost_tracking_3, cost_tracking_4, cost_tracking_5])


@pytest.fixture
def example_new_user_data():
    return User([], [])


@pytest.fixture
def example_available_restaurants():
    restaurant_1 = Restaurant("1", Cuisine.NORTH_INDIAN, 3, 4.7, True, date(2022, 12, 25))
    restaurant_2 = Restaurant("2", Cuisine.SOUTH_INDIAN, 1, 4.1, True, date(2023, 2, 22))
    restaurant_3 = Restaurant("3", Cuisine.CHINESE, 2, 3.7, False, date(2022, 1, 5))
    restaurant_4 = Restaurant("4", Cuisine.NORTH_INDIAN, 5, 2.4, False, date(2023, 1, 2))
    restaurant_5 = Restaurant("5", Cuisine.SOUTH_INDIAN, 4, 3.8, False, date(2022, 10, 15))
    restaurant_6 = Restaurant("6", Cuisine.CHINESE, 1, 4.3, True, date(2022, 8, 11))
    restaurant_7 = Restaurant("7", Cuisine.NORTH_INDIAN, 1, 4.9, True, date(2023, 4, 16))
    restaurant_8 = Restaurant("8", Cuisine.SOUTH_INDIAN, 5, 5, True, date(2022, 11, 9))
    restaurant_9 = Restaurant("9", Cuisine.CHINESE, 3, 4.5, True, date(2021, 12, 25))
    restaurant_10 = Restaurant("10", Cuisine.NORTH_INDIAN, 2, 4.0, True, date(2022, 7, 1))
    restaurant_11 = Restaurant("11", Cuisine.SOUTH_INDIAN, 5, 3.9, False, date(2022, 10, 12))
    restaurant_12 = Restaurant("12", Cuisine.CHINESE, 4, 4.8, True, date(2022, 12, 30))
    restaurant_13 = Restaurant("13", Cuisine.NORTH_INDIAN, 2, 1.3, False, date(2022, 9, 16))
    restaurant_14 = Restaurant("14", Cuisine.SOUTH_INDIAN, 2, 2.8, False, date(2022, 3, 30))
    restaurant_15 = Restaurant("15", Cuisine.CHINESE, 4, 3.3, False, date(2022, 5, 31))
    restaurant_16 = Restaurant("16", Cuisine.SOUTH_INDIAN, 3, 4.6, True, date(2022, 6, 13))
    restaurant_17 = Restaurant("17", Cuisine.SOUTH_INDIAN, 3, 4.7, True, date(2023, 2, 11))
    restaurant_18 = Restaurant("18", Cuisine.CHINESE, 2, 4.8, True, date(2023, 3, 21))
    restaurant_19 = Restaurant("19", Cuisine.NORTH_INDIAN, 3, 4.8, True, date(2023, 4, 1))

    available_restaurants = [restaurant_1, restaurant_2, restaurant_3, restaurant_4, restaurant_5, restaurant_6,
                             restaurant_7, restaurant_8, restaurant_9, restaurant_10, restaurant_11, restaurant_12,
                             restaurant_13, restaurant_14, restaurant_15, restaurant_16, restaurant_17, restaurant_18,
                             restaurant_19]

    return available_restaurants


def test_get_restaurants_recommendations_for_old_user(example_old_user_data, example_available_restaurants):
    print(RestaurantRecommendations().get_restaurants_recommendations(example_old_user_data, example_available_restaurants))
    assert True


def test_get_restaurants_recommendations_for_new_user(example_new_user_data, example_available_restaurants):
    print(RestaurantRecommendations().get_restaurants_recommendations(example_new_user_data, example_available_restaurants))
    assert True
