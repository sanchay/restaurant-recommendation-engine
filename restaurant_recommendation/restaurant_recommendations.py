from datetime import date


class RestaurantRecommendations:

    def get_restaurants_recommendations(self, user, available_restaurants):
        # Getting Primary & Secondary Cuisines
        no_of_primary_cuisines = 1
        no_of_secondary_cuisines = 2
        primary_cuisines, secondary_cuisines = self._get_primary_secondary_cuisines(no_of_primary_cuisines,
                                                                                    no_of_secondary_cuisines,
                                                                                    user.cuisines)

        # Getting Primary & Secondary Cost Brackets
        no_of_primary_cost_brackets = 1
        no_of_secondary_cost_brackets = 2
        primary_cost_brackets, secondary_cost_brackets = self._get_primary_secondary_cost_brackets(
            no_of_primary_cost_brackets,
            no_of_secondary_cost_brackets,
            user.cost_bracket)

        top_restaurants = []
        no_of_rules = 8
        temp_top_restaurants = [set() for i in range(no_of_rules)]
        no_of_top_restaurants = 100
        available_restaurants_set = set(available_restaurants)

        # Applying first 3 rules to available restaurants and storing them in temp_top_restaurants indices
        for restaurant in available_restaurants:
            if restaurant.cuisine in primary_cuisines and restaurant.cost_bracket in primary_cost_brackets and restaurant.rating >= 4:
                temp_top_restaurants[0].add(restaurant.restaurant_id)
                available_restaurants_set.remove(restaurant)
            elif restaurant.cuisine in primary_cuisines and restaurant.cost_bracket in secondary_cost_brackets and restaurant.rating >= 4.5:
                temp_top_restaurants[1].add(restaurant.restaurant_id)
                available_restaurants_set.remove(restaurant)
            elif restaurant.cuisine in secondary_cuisines and restaurant.cost_bracket in primary_cost_brackets and restaurant.rating >= 4.5:
                temp_top_restaurants[2].add(restaurant.restaurant_id)
                available_restaurants_set.remove(restaurant)

        # Getting 4 top-rated new restaurants
        if len(available_restaurants_set) >= 4:
            new_restaurants = [x for x in available_restaurants_set if x.onboarded_time > date(2023, 3, 15)]
            top_rated_new_restaurants = sorted(list(new_restaurants),
                                               key=lambda x: x.rating,
                                               reverse=True)[:4]

            temp_top_restaurants[3] = [x.restaurant_id for x in top_rated_new_restaurants]

        # Applying rest of the rules 4-8 and storing their respective results on remaining restaurants
        for restaurant in available_restaurants_set:

            # De-duplicating  top-rated new restaurants below, this if case will be triggered 4 times
            if restaurant.restaurant_id in temp_top_restaurants[3]:
                continue

            if restaurant.cuisine in primary_cuisines and restaurant.cost_bracket in primary_cost_brackets:
                temp_top_restaurants[4].add(restaurant.restaurant_id)
            elif restaurant.cuisine in primary_cuisines and restaurant.cost_bracket in secondary_cost_brackets:
                temp_top_restaurants[5].add(restaurant.restaurant_id)
            elif restaurant.cuisine in secondary_cuisines and restaurant.cost_bracket in primary_cost_brackets:
                temp_top_restaurants[6].add(restaurant.restaurant_id)
            else:
                temp_top_restaurants[7].add(restaurant.restaurant_id)

        # Making list feed to display to user from temp_top_restaurants
        for restaurant_set in temp_top_restaurants:
            if len(restaurant_set) > no_of_top_restaurants - len(top_restaurants):
                top_restaurants += list(restaurant_set)[:(no_of_top_restaurants - len(top_restaurants))]
                break
            top_restaurants += list(restaurant_set)

        return top_restaurants

    # Function to extract m, n no. of primary, secondary items from list based on no_of_orders
    def _get_primary_secondary_items(self, no_of_primary_items, no_of_secondary_items, item_list):
        primary_items = set()
        secondary_items = set()
        item_list.sort(key=lambda x: x.no_of_orders, reverse=True)
        if no_of_primary_items < len(item_list):
            no_of_primary_items_to_add = no_of_primary_items
            if no_of_primary_items_to_add + no_of_secondary_items > len(item_list):
                no_of_secondary_items = len(item_list) - no_of_primary_items_to_add
        else:
            no_of_primary_items_to_add = len(item_list)
            no_of_secondary_items = 0
        for i in range(no_of_primary_items_to_add):
            primary_items.add(item_list[i])
        for i in range(no_of_primary_items_to_add, no_of_primary_items_to_add + no_of_secondary_items):
            secondary_items.add(item_list[i])
        return primary_items, secondary_items

    def _get_primary_secondary_cuisines(self, no_of_primary_cuisines, no_of_secondary_cuisines, cuisines_tracking_list):
        primary_items, secondary_items = self._get_primary_secondary_items(no_of_primary_cuisines,
                                                                           no_of_secondary_cuisines,
                                                                           cuisines_tracking_list)
        primary_cuisines = {x.cuisine for x in primary_items}
        secondary_cuisines = {x.cuisine for x in secondary_items}
        return primary_cuisines, secondary_cuisines

    def _get_primary_secondary_cost_brackets(self, no_of_primary_cost_brackets, no_of_secondary_cost_brackets, cost_tracking_list):
        primary_items, secondary_items = self._get_primary_secondary_items(no_of_primary_cost_brackets,
                                                                           no_of_secondary_cost_brackets,
                                                                           cost_tracking_list)
        primary_cost_brackets = {x.type for x in primary_items}
        secondary_cost_brackets = {x.type for x in secondary_items}
        return primary_cost_brackets, secondary_cost_brackets
