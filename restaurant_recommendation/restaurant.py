class Restaurant:
    def __init__(self, restaurant_id, cuisine, cost_bracket, rating, is_recommended, onboarded_time):
        self.restaurant_id = restaurant_id
        self.cuisine = cuisine
        self.cost_bracket = cost_bracket
        self.rating = rating
        self.is_recommended = is_recommended
        self.onboarded_time = onboarded_time
